from django.shortcuts import render

# Create your views here.


def dynamic(request, template="index"):
    template += ".html"
    return render(request,  template)